/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.sarocha.oxoop;

/**
 *
 * @author Sarocha
 */
public class OXOOP {

    public static void main(String[] args) {
        Game game = new Game();
        game.showWelcome();
        game.newboard();
        while (true) {
            game.showTable();
            game.showTurn();
            game.inputRowCol();
            if (game.isFinish()) {
               game.showTable();
               game.showResult();
               game.showStat();
               if (game.showContinuePlaying()==false) {
                   System.out.println("Good bye");
                   break;
               }
               game.newboard();
            }
        }

    }
}
