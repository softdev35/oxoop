/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sarocha.oxoop;

/**
 *
 * @author Sarocha
 */
public class Board {

    private char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentPlayer;
    private Player O;
    private Player X;
    private int count;
    public boolean win = false;
    public boolean draw = false;

    public Board(Player O, Player X) {
        this.O = O;
        this.X = X;
        this.currentPlayer = O;
        this.count = 0;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public Player getO() {
        return O;
    }

    public Player getX() {
        return X;
    }

    public int getCount() {
        return count;
    }

    private void switchPlayer() {
        if (currentPlayer == O) {
            currentPlayer = X;
        } else {
            currentPlayer = O;
        }
    }

    public boolean setRowCol(int row, int col) {
        if (isWin() || isDraw()) {
            return false;
        }
        if (row > 3 || col > 3 || row < 1 || col < 1) {
            return false;
        }
        if (table[row - 1][col - 1] != '-') {
            return false;
        }
        table[row - 1][col - 1] = currentPlayer.getSymbol();
        if (checkWin(row, col)) {
            if (this.currentPlayer == O) {
                O.win();
                X.loss();
            }else {
                X.win();
                O.loss();
            }
            this.win = true;
            return true;
        }
        if (checkDraw()) {
            O.draw();
            X.draw();
            this.draw = true;
            return true;
        }
        count++;
        switchPlayer();
        return true;
    }

    public boolean checkDraw() {
        if (count == 8) {
            return true;
        }
        return false;
    }

    public boolean isWin() {
        return win;
    }

    public boolean isDraw() {
        return draw;
    }

    public boolean checkWin(int row, int col) {
        if (checkVertical(table, currentPlayer.getSymbol(), col)) {
            return true;
        } else if (checkHorizontal(table, currentPlayer.getSymbol(), row)) {
            return true;
        } else if (checkDiagonal(table, currentPlayer.getSymbol())) {
            return true;
        }
        return false;
    }

    public boolean checkVertical(char[][] table, char currentPlayer, int col) {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public boolean checkHorizontal(char[][] table, char currentPlayer, int row) {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public boolean checkDiagonal(char[][] table, char currentPlayer) {
        if (checkDiagonal1()) {
            return true;
        } else if (checkDiagonal2()) {
            return true;
        }
        return false;
    }

    public boolean checkDiagonal1() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkDiagonal2() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }
}
